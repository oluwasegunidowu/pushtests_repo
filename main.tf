 resource "aws_instance" "webserver"{
    ami = "ami-0103f211a154d64a6"
    instance_type = "t2.micro"

provisioner "remote-exec"{
   inline = [
              "sudo apt update",
              "sudo apt install nginx -y",
              "systemctl enable nginx",
              "systemctl start nginx",
            ]
}
    tags = {
        Name = "webserver"
        Description = "An NginX webServer on ubuntu"
    }

     user_data = <<-EOF
                    #!/bin/bash
                    sudo apt update
                    sudo apt install nginx -y
                    systemctl enable nginx
                    systemctl start nginx
                    EOF

      connection {
         type = "ssh"
         host = self.public_ip
         user = "ubuntu"
         private_key = file("/Users/noob/.ssh/web/solo-kp.pem") 
      }

  key_name = aws_key_pair.webserver_kp.id 
  vpc_security_group_ids = [aws_security_group.ssh_access.id]  
 }

 resource "aws_key_pair" "webserver_kp" {
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAmeFDpWYisVh1QXLjpfaNNbeI94EW52iG0bLaMqdsvj/N6xdVZbMeu1WZXg8EMitU8A4cOglMrSOeoHq562k3oqGXoYyC36/QsuZJqNnNIzqv5RclAmFWPAz5qcPHxHuUkhvepjqfAw+XC1vbBfsCMSlf7kWkVo/SywrmmyGiIlIRPmBDts+cq8mXqMynm4arccRVXHdfoGfkFbMaCzmUVfkqE3S7xjJnTEmd0s5fOKYDONiQ1TM5SMwtNLXwJrPdtxd7OF9st/S/46kFhJEKIuYTihQK/oREz73g5pJXbJC3QxH3ypkyJm0QoFq+pz0QFRAmsVnvfCW1mNYysEd/tvADQGZZqZ/40RXqNOOxJlzU1I/VOKISOWYwXIIBrXtal3AgqgrcZrE4Kmx6Y8OXtDqTxGqDaX+OZ1xnPbgp59odJ5/gY64X6VW7X6wdkJv8VC0nefjPasXwAbSj1LWEmiEsoFwFKMZU1OGQ1jc6+q6keK6DplnoEmeYsvhrYEc= noob@Oluwaseguns-MacBook-Pro.local"
 }

 resource "aws_security_group" "ssh_access" {
    name = "ssh-access"
    description     = "Allow ssh access from the internet"
    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
 }

 output publicip {
    value = aws_instance.webserver.public_ip
 }